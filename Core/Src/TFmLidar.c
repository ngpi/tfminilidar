/*
 * tfMiniLidar.c
 *
 *  Created on: Mar 18, 2020
 *      Author: NG
 */

#include "main.h"
#include "TFmLidar.h"
#include "string.h"
#include "stdio.h"





uint8_t rxCnt=0,msgLen=0;
uint8_t rxByteTfMiniUART,TfMiniUARTrxBuf[10],TfMiniData[10],tfMiniDataReady=0;
uint32_t tickstartrx;
TF_MINI_CONFIG tfLidarConfig;
#define TfMiniUART_RX_TIMEOUT 200
#define TfMiniUART_INIT_RX_TIMEOUT 2000


/* error handler for the configuration routine */
void TfMini_config_Error_Handler(uint8_t err)
{
	uint8_t buf[50];
switch(err)
{
	case 1:
		snprintf((char *)buf,50,"\nTfMini LIdar config reply error ...!");
		break;
	case 2:
		snprintf((char *)buf,50,"\nTfMini LIdar init time out error ...!");
		break;
	case 3:
		snprintf((char *)buf,50,"\nTfMini LIdar UR time out error ...!");
		break;
	case 4:
		snprintf((char *)buf,50,"\nTfMini LIdar UR reply error ...!");
		break;
	case 5:
		snprintf((char *)buf,50,"\nTfMini LIdar unit reply error ...!");
		break;
	case 6:
		snprintf((char *)buf,50,"\nTfMini LIdar unit timeout error ...!");
		break;
}
#ifdef TfMiniReportUART
HAL_UART_Transmit(&TfMiniReportUART, (uint8_t*)buf, strlen((char *)buf), 1000);
#endif
}


/* Function:  configuration for Lidar
 *
 * Param - Pointer to TF_MINI_CONFIG with configuration defined
 * Return - 0- if successful
 * 			non zero - if error
 */
uint8_t configTfMiniLIdar(TF_MINI_CONFIG * config)
{
	uint8_t txBuf[10],i;
	uint32_t tickstartcfg;
	tickstartcfg = HAL_GetTick();
	HAL_UART_Abort(&TfMiniUART);
	HAL_UART_Receive_IT(&TfMiniUART,  &rxByteTfMiniUART, 1);
	while(tfMiniDataReady==0)
	{
		if((HAL_GetTick() - tickstartcfg) > TfMiniUART_INIT_RX_TIMEOUT)
			return 2;
	}
	tfMiniDataReady=0;
	/* set update rate */
	txBuf[0]=CMD_HDR;
	txBuf[1]=6; /* length */
	txBuf[2]=CMD_UPDATE_RATE;
	txBuf[3]=(config->updateRate&0xff);
	txBuf[4]=(config->updateRate&0xff00)>>8;

	txBuf[5]=0;
	for(i=0;i<5;i++)
	{
		txBuf[5]+=txBuf[i];
	}
	if(HAL_UART_Transmit(&TfMiniUART, (uint8_t*)txBuf, 6, 1000)!=HAL_OK)return 1;
	HAL_UART_Abort(&TfMiniUART);
	HAL_UART_Receive_IT(&TfMiniUART,  &rxByteTfMiniUART, 1);
	tickstartcfg = HAL_GetTick();
	while(tfMiniDataReady==0)
	{
		if((HAL_GetTick() - tickstartcfg) > TfMiniUART_RX_TIMEOUT)
			return 3;
	}
	tfMiniDataReady=0;
	for(i=0;i<6;i++)
		{
			if(TfMiniData[i]!=txBuf[i])break;
		}
	if(i!=6)
		return 4;
	/* set unit */
	txBuf[0]=CMD_HDR;
	txBuf[1]=5; /* length */
	txBuf[2]=CMD_SET_MEAS_UNIT;
	txBuf[3]=(config->setUnit);


	txBuf[4]=0;
	for(i=0;i<4;i++)
	{
		txBuf[4]+=txBuf[i];
	}
	if(HAL_UART_Transmit(&TfMiniUART, (uint8_t*)txBuf, 5, 1000)!=HAL_OK)return 1;
	tickstartcfg = HAL_GetTick();
	while(tfMiniDataReady==0)
	{
		if((HAL_GetTick() - tickstartcfg) > TfMiniUART_RX_TIMEOUT)
			return 6;
	}
	tfMiniDataReady=0;
	for(i=0;i<5;i++)
		{
			if(TfMiniData[i]!=txBuf[i])break;
		}
	if(i!=5)
		return 5;
	/* set enable/Disable */

	/* Set baud rate */

	return 0;
}

/* Function: Start Lidar with some basic configuration
 * 			 This function should be called at the beginning of the program
 * Param - None
 * Return - None
 */
void startTfMiniLidar()
{

	 HAL_UART_Receive_IT(&TfMiniUART,  &rxByteTfMiniUART, 1);

	 tfLidarConfig.enOutput=ENABLE;		/* enable output */
	 tfLidarConfig.setUnit=UNIT_MM;		/* set the unit */
	 tfLidarConfig.updateRate=3; 		/* 1 samples per second */
	 uint8_t err=configTfMiniLIdar(&tfLidarConfig);
	 if(err!=0)
	 {
		 TfMini_config_Error_Handler(err);
	 }
}

/* Function: To read data from Mini Lidar
 * Param - pointer to TF_MINI_DATA data structure to read the data
 * Return - 1 if data is not ready
 * 		  - 0 if data is ready
 */

uint8_t rdTfMiniData(TF_MINI_DATA * tfData)
{
	if(tfMiniDataReady==0)return 1;
	tfMiniDataReady=0;
	if(TfMiniData[0]!=0x59)return 1; /* only take data packet */
	/* verify the checksum */
	tfData->distance = * ((uint16_t*)&TfMiniData[2]);
	tfData->sigStrength=*((uint16_t*)&TfMiniData[4]);
	tfData->temperature=((*((uint16_t*)&TfMiniData[6]))/8)-256;
	return 0;

}

/* Error call back - called when there is error in serial interrupt */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	rxByteTfMiniUART=(uint8_t)huart->Instance->RDR;
	huart->ErrorCode=0;/* clear  error */
	HAL_UART_Receive_IT(&TfMiniUART,  &rxByteTfMiniUART, 1);
}

/* Receive complete interrupt call back */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if(huart!=&TfMiniUART)return ;

		if(rxCnt==0)
		{
			tickstartrx = HAL_GetTick();
			TfMiniUARTrxBuf[rxCnt]=rxByteTfMiniUART;
			switch(rxByteTfMiniUART)
			{

				case 0x59:
				{
					msgLen=9;
					break;
				}
				case 0x5A:
				{
					msgLen=0xFF;
					break;
				}
				default:
					msgLen=0;
					HAL_UART_Receive_IT(&TfMiniUART,  &rxByteTfMiniUART, 1);
					return ;
			}
			rxCnt++;
			HAL_UART_Receive_IT(&TfMiniUART,  &rxByteTfMiniUART, 1);
			return ;
		}
		if((HAL_GetTick() - tickstartrx) > TfMiniUART_RX_TIMEOUT)
		{
			rxCnt=0;
			return ;
		}
		if(rxCnt==1)
		{
			TfMiniUARTrxBuf[rxCnt]=rxByteTfMiniUART;
			rxCnt++;
			if(msgLen==0xFF  && rxByteTfMiniUART!=0x59  ){msgLen=rxByteTfMiniUART;}
			HAL_UART_Receive_IT(&TfMiniUART, &rxByteTfMiniUART, 1);
			return ;
		}
		TfMiniUARTrxBuf[rxCnt]=rxByteTfMiniUART;
		rxCnt++;
		if(rxCnt==msgLen)
		{
			for(rxCnt=0;rxCnt<msgLen;rxCnt++)TfMiniData[rxCnt]=TfMiniUARTrxBuf[rxCnt];
			rxCnt=0;
			tfMiniDataReady=1;
			//return ;
		}
		HAL_UART_Receive_IT(&TfMiniUART, &rxByteTfMiniUART, 1);
}





