/*
 * tfMiniLidar.h
 *
 *  Created on: Mar 18, 2020
 *      Author: NG
 */

#ifndef INC_TFMINILIDAR_H_
#define INC_TFMINILIDAR_H_

/* extern here the proper UART to be used in the system*/
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

#define TfMiniUART huart1
/* if report UART is not used then comment the following define */
#define TfMiniReportUART huart2


typedef struct tf_mini_data
{
	uint16_t distance;
	uint16_t sigStrength;
	uint16_t temperature;
}TF_MINI_DATA;


typedef struct tf_mini_config
{
	uint16_t updateRate;
	uint32_t baudRate;
	uint8_t setUnit;
	uint8_t enOutput;
}TF_MINI_CONFIG;

#define CMD_HDR 0x5A
#define CMD_GET_FW_VER 0x1
#define CMD_SYS_RST 0x2
#define CMD_UPDATE_RATE 0x3
#define CMD_SET_MEAS_UNIT 0x5
#define CMD_SET_BAUD 0x6
#define CMD_EN_DIS_OP 0x7
#define CMD_FACT_DEFA 0x10
#define CMD_SAVE_CONFIG 0x11

#define UNIT_CM 0x1
#define UNIT_MM 0x6
#define ENABLE 0x1
#define DISABLE 0x0




extern uint8_t TfMiniData[10];
extern uint8_t rxByteUART1;
extern TF_MINI_CONFIG tfLidarConfig;


uint8_t rdTfMiniData(TF_MINI_DATA * tfData);		/* Call this function to read the mini lidar data */
void startTfMiniLidar(void);						/* Call this function to start the mili Lidar */
uint8_t configTfMiniLIdar(TF_MINI_CONFIG * config); /* Function to configure the mili Lidar */

#endif /* INC_TFMINILIDAR_H_ */
